package com.junior.stronger197.tfs2018homework2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {

    private val topList = mutableListOf<Chip>()
    private val bottomList = mutableListOf<Chip>()

    private val metroStationsList = arrayListOf(
            "Epping",
            "Theydon Bois",
            "Debden",
            "Loughton",
            "Chigwell",
            "Roding Valley",
            "Woodford",
            "Snaresbrook",
            "Hainault",
            "Fairlop",
            "Barkingside",
            "Newbury Park",
            "Gants Hill",
            "Redbridge",
            "Wanstead",
            "Leytonstone",
            "Leyton",
            "Stratford",
            "Mile End",
            "Bank"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // configure chips
        metroStationsList.forEach { metroStationString ->
            val chip = Chip(this)
            chip.layoutParams = ViewGroup.MarginLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
            chip.text = metroStationString
            chip.setOnClickListener {
                if(it.parent == topChipGroup) {
                    topList.remove(it as Chip)
                    bottomList.add(it)
                } else if (it.parent == bottomChipGroup){
                    bottomList.remove(it as Chip)
                    topList.add(it)
                }

                topChipGroup.removeAllViews()
                bottomChipGroup.removeAllViews()

                setChipsView(topChipGroup, topList)
                setChipsView(bottomChipGroup, bottomList)
            }

            topList.add(chip)
        }

        setChipsView(topChipGroup, topList)
        setChipsView(bottomChipGroup, bottomList)
    }

    private fun setChipsView(group : RightAlignmentChipsGroup, items : List<Chip>) {
        items.forEach {
            group.addView(it)
        }
    }
}
