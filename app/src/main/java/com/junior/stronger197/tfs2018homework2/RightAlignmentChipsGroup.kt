package com.junior.stronger197.tfs2018homework2

import android.content.Context
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.ViewGroup
import android.view.View


class RightAlignmentChipsGroup : ViewGroup {

    fun convertDpToPixel(dp: Int, context: Context) = dp * (context.resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)


    private val counterList = mutableListOf<Int>()

    constructor(context: Context?) : this(context, null)

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        for (i in 0 until childCount) {
            val child = getChildAt(i)
            val w = layoutParams.width
            val h = layoutParams.height

            val childWidthSpec = View.MeasureSpec.makeMeasureSpec(w, View.MeasureSpec.AT_MOST)
            val childHeightSpec = View.MeasureSpec.makeMeasureSpec(h, View.MeasureSpec.AT_MOST)
            child.measure(childWidthSpec, childHeightSpec)
        }

        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {

        val spacing = convertDpToPixel(4, context)

        val availableWidth = right - left
        var widthUsed = 0
        var totalRowsCount = 1 // total rows

        for(i in 0 until childCount) {
            val childWidth = getChildAt(i).measuredWidth + paddingStart + paddingEnd + (spacing * 2)

            if(childWidth + widthUsed <= availableWidth) {
                widthUsed += childWidth
            } else {
                totalRowsCount++
                widthUsed = 0
                widthUsed += childWidth
            }

        }

        for(i in 0 until totalRowsCount) {
            counterList.add(0)
        }

        var currentRow = 0
        widthUsed = 0
        for(i in 0 until childCount) {
            val childWidth = getChildAt(i).measuredWidth + paddingStart + paddingEnd + (spacing * 2)

            if(childWidth + widthUsed <= availableWidth) {
                widthUsed += childWidth
                counterList[currentRow] = i + 1
            } else {
                currentRow++
                widthUsed = 0
                widthUsed += childWidth
                counterList[currentRow] = i + 1
            }
        }

        for(i in 0 until counterList.size) {
            widthUsed = 0
            when (i) {
                0 -> {
                    for(j in counterList[i] - 1 downTo 0) {
                        val childToDraw = getChildAt(j)
                        if(childToDraw != null) {
                            childToDraw.layout(
                                    right - left - paddingEnd - widthUsed - childToDraw.measuredWidth - spacing,
                                    childToDraw.measuredHeight * i,
                                    right - left - paddingEnd - widthUsed - spacing,
                                    childToDraw.measuredHeight * (i + 1)
                            )
                            widthUsed += childToDraw.measuredWidth + spacing * 2
                        }
                    }
                }
                else -> {
                    for(j in counterList[i] - 1 downTo counterList[i-1]) {
                        val childToDraw = getChildAt(j)
                        if(childToDraw != null) {
                            childToDraw.layout(
                                    right - left - widthUsed - childToDraw.measuredWidth - spacing,
                                    childToDraw.measuredHeight * i + (spacing * 2 * i),
                                    right - left - widthUsed - spacing,
                                    childToDraw.measuredHeight * (i + 1) + (spacing * 2 * i)
                            )
                            widthUsed += childToDraw.measuredWidth + spacing * 2
                        }
                    }
                }
            }
        }

    }
}